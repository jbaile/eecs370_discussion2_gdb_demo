#include "stdlib.h"

void func() {
    int* ptr = NULL;
    *ptr = 10; // It should crash here
}

int main() {
    func();
    return 0;
}
